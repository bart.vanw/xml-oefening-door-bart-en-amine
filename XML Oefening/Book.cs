﻿namespace XML_Oefening
{
    class Book
    {
        private string _title;
        private string _publisher;

        public Book()
        {

        }

        public Book(string titel, string uitgever)
        {
            Title = titel;
            Publisher = uitgever;
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Publisher
        {
            get { return _publisher; }
            set { _publisher = value; }
        }

        public override string ToString()
        {
            return "Titel: " + Title.PadRight(30) + "Uitgever: " + Publisher.PadRight(30);
        }
    }
}
