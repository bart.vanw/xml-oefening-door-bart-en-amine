﻿
namespace XML_Oefening
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnXMLBooks1 = new System.Windows.Forms.Button();
            this.btnXMLBooks2 = new System.Windows.Forms.Button();
            this.btnXMLBooks3 = new System.Windows.Forms.Button();
            this.lbXML = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbBoekenLijst = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnXMLBooks1
            // 
            this.btnXMLBooks1.Location = new System.Drawing.Point(13, 13);
            this.btnXMLBooks1.Name = "btnXMLBooks1";
            this.btnXMLBooks1.Size = new System.Drawing.Size(156, 54);
            this.btnXMLBooks1.TabIndex = 0;
            this.btnXMLBooks1.Text = "Lees xmlbooks1.xml";
            this.btnXMLBooks1.UseVisualStyleBackColor = true;
            this.btnXMLBooks1.Click += new System.EventHandler(this.btnXMLBooks1_Click);
            // 
            // btnXMLBooks2
            // 
            this.btnXMLBooks2.Location = new System.Drawing.Point(190, 13);
            this.btnXMLBooks2.Name = "btnXMLBooks2";
            this.btnXMLBooks2.Size = new System.Drawing.Size(156, 54);
            this.btnXMLBooks2.TabIndex = 1;
            this.btnXMLBooks2.Text = "Lees xmlbooks2.xml";
            this.btnXMLBooks2.UseVisualStyleBackColor = true;
            this.btnXMLBooks2.Click += new System.EventHandler(this.btnXMLBooks2_Click);
            // 
            // btnXMLBooks3
            // 
            this.btnXMLBooks3.Location = new System.Drawing.Point(368, 13);
            this.btnXMLBooks3.Name = "btnXMLBooks3";
            this.btnXMLBooks3.Size = new System.Drawing.Size(156, 54);
            this.btnXMLBooks3.TabIndex = 2;
            this.btnXMLBooks3.Text = "Schrijf xmlbooks3.xml";
            this.btnXMLBooks3.UseVisualStyleBackColor = true;
            this.btnXMLBooks3.Click += new System.EventHandler(this.btnXMLBooks3_Click);
            // 
            // lbXML
            // 
            this.lbXML.FormattingEnabled = true;
            this.lbXML.ItemHeight = 15;
            this.lbXML.Location = new System.Drawing.Point(13, 83);
            this.lbXML.Name = "lbXML";
            this.lbXML.Size = new System.Drawing.Size(511, 319);
            this.lbXML.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 433);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Mijn boekenlijst:";
            // 
            // lbBoekenLijst
            // 
            this.lbBoekenLijst.FormattingEnabled = true;
            this.lbBoekenLijst.ItemHeight = 15;
            this.lbBoekenLijst.Location = new System.Drawing.Point(13, 451);
            this.lbBoekenLijst.Name = "lbBoekenLijst";
            this.lbBoekenLijst.Size = new System.Drawing.Size(511, 169);
            this.lbBoekenLijst.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 636);
            this.Controls.Add(this.lbBoekenLijst);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbXML);
            this.Controls.Add(this.btnXMLBooks3);
            this.Controls.Add(this.btnXMLBooks2);
            this.Controls.Add(this.btnXMLBooks1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnXMLBooks1;
        private System.Windows.Forms.Button btnXMLBooks2;
        private System.Windows.Forms.Button btnXMLBooks3;
        private System.Windows.Forms.ListBox lbXML;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbBoekenLijst;
    }
}

