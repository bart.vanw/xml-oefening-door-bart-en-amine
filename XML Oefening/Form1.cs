﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;

namespace XML_Oefening
{

    public partial class Form1 : Form
    {
        List<Book> LijstBoeken = new List<Book>();
        Book boek;
        public Form1()
        {
            InitializeComponent();
        }
        private void btnXMLBooks1_Click(object sender, EventArgs e)
        {
            string pad = Environment.CurrentDirectory + @"\xmlbooks1.xml";
            string element = "";
            string titel = "";
            string uitgever = "";

            using (XmlReader xmlIn = XmlReader.Create(pad))
            {
                while (xmlIn.Read())
                {
                    if (xmlIn.NodeType == XmlNodeType.Element)
                    {
                        element = xmlIn.Name;
                        lbXML.Items.Add(xmlIn.Name);

                        if (xmlIn.GetAttribute("datumtijd") != null)
                        {
                            lbXML.Items.Add(xmlIn.GetAttribute("datumtijd"));
                        }
                    }
                    else if (xmlIn.NodeType == XmlNodeType.Text)
                    {
                        lbXML.Items.Add(xmlIn.Value);
                        if (element == "title")
                        {
                            titel = xmlIn.Value;
                        }
                        else if (element == "publisher")
                        {
                            uitgever = xmlIn.Value;
                        }
                    }
                    else if (xmlIn.NodeType == XmlNodeType.EndElement)
                    {
                        lbXML.Items.Add("/" + xmlIn.Name);
                        if (xmlIn.Name == "book")
                        {
                            boek = new Book(titel, uitgever);
                            LijstBoeken.Add(boek);
                            lbBoekenLijst.Items.Add(boek.ToString());
                        }
                    }
                }
            }
        }
        private void btnXMLBooks2_Click(object sender, EventArgs e)
        {
            lbXML.Items.Clear();

            string pad = Environment.CurrentDirectory + @"\xmlbooks2.xml";
            string element = "";
            string titel = "";
            string uitgever = "";

            using (XmlReader xmlIn = XmlReader.Create(pad))
            {
                while (xmlIn.Read())
                {
                    if (xmlIn.NodeType == XmlNodeType.Element)
                    {
                        element = xmlIn.Name;
                        lbXML.Items.Add(xmlIn.Name);

                        if (xmlIn.GetAttribute("datumtijd") != null)
                        {
                            lbXML.Items.Add(xmlIn.GetAttribute("datumtijd"));
                        }
                    }
                    else if (xmlIn.NodeType == XmlNodeType.Text)
                    {
                        lbXML.Items.Add(xmlIn.Value);
                        if (element == "title")
                        {
                            titel = xmlIn.Value;
                        }
                        else if (element == "publisher")
                        {
                            uitgever = xmlIn.Value;
                        }
                    }
                    else if (xmlIn.NodeType == XmlNodeType.EndElement)
                    {
                        lbXML.Items.Add("/" + xmlIn.Name);
                        if (xmlIn.Name == "book")
                        {
                            boek = new Book(titel, uitgever);
                            LijstBoeken.Add(boek);
                            lbBoekenLijst.Items.Add(boek.ToString());
                        }
                    }
                }
            }
        }
        private void btnXMLBooks3_Click(object sender, EventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create(Environment.CurrentDirectory + @"\allbooks.xml"))
            {
                writer.WriteStartElement("books");
                writer.WriteAttributeString("datumtijd", DateTime.Now.ToString());
                foreach (Book boek in LijstBoeken)
                {
                    writer.WriteStartElement("book");
                    writer.WriteElementString("title", boek.Title);
                    writer.WriteElementString("publisher", boek.Publisher);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }
    }
}
